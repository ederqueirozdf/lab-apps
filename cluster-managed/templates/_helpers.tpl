{{/*
Expand the name of the chart.
*/}}
{{- define "cluster-managed.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "cluster-managed.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "cluster-managed.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "cluster-managed.labels" -}}
helm.sh/chart: {{ include "cluster-managed.chart" . }}
{{ include "cluster-managed.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "cluster-managed.selectorLabels" -}}
app.kubernetes.io/name: {{ include "cluster-managed.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "cluster-managed.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "cluster-managed.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{- define "cluster-managed.installconfig" -}}
apiVersion: v1
metadata:
  name: '{{ .Values.cluster.name }}'
baseDomain: {{ .Values.cluster.baseDomain }}
proxy:
  httpProxy: {{ .Values.proxy.httpProxy }}
  httpsProxy: {{ .Values.proxy.httpsProxy }}
  noProxy: {{ .Values.proxy.noProxy }}
controlPlane:
  hyperthreading: Enabled
  name: master
  replicas: 3
  platform:
    vsphere:
{{ toYaml .Values.controlPlane | indent 6 }}
compute:
{{- range .Values.compute}}
{{- if eq .name "worker"}}
- {{ toYaml (omit . "labels" "taints") | indent 2 | trim }}
{{- else -}}
- hyperthreading: Enabled
  name: 'worker'
  replicas: 3
  platform:
    vsphere:
      cpus:  4
      coresPerSocket:  2
      memoryMB:  16384
      osDisk:
        diskSizeGB: 120
{{- end }}
{{- end }}
platform:
  vsphere:
    vCenter: {{ .Values.cluster.vsphere.vCenter }}
    username: {{ .Values.cluster.vsphere.username | b64dec }}
    password: {{ .Values.cluster.vsphere.password | b64dec }}
    datacenter: {{ .Values.cluster.vsphere.datacenter }}
    defaultDatastore: {{ .Values.cluster.vsphere.defaultDatastore }}
    folder: {{ .Values.cluster.vsphere.folder }}
    cluster: {{ .Values.cluster.vsphere.cluster }}
    apiVIP: {{ .Values.cluster.apiVIP }}
    ingressVIP: {{ .Values.cluster.ingressVIP }}
    network: {{ .Values.cluster.vsphere.network }}
    clusterOSImage: {{ .Values.cluster.clusterOSImage }}
networking:
{{ toYaml .Values.networking | indent 2 }}
imageContentSources:
{{ toYaml .Values.imageContentSources | indent 2 }}
pullSecret: "" 
sshKey: |-
{{ .Values.sshKey.pub  | indent 2 }}     
additionalTrustBundle: |-
{{ .Values.additionalTrustBundle  | indent 2 }}
{{- end }}